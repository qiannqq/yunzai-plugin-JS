import { segment } from "icqq";
import plugin from '../../lib/plugins/plugin.js';
import fetch from "node-fetch";
import common from'../../lib/common/common.js'

/**
 * 作者：千奈千祁
 * Gitee主页：Gitee.com/QianNQQ
 * 个人博客：blog.moqy.fun
 * QQ：2632139786
 * 调用的API接口：api.shanhe.kim
 */


export class example2 extends plugin {
    constructor () {
      super({
        name: '抽签',
        dsc: '每日随机抽签',
        event: 'message',
        priority: -1,
        rule: [
          {
            reg: '^#?抽签$|^#?求签$|^#?今日运势$',
            fnc: '随机抽签'
          }
        ]
      })
    }
    async 随机抽签(e){
        const currentDate = new Date();
        const year = currentDate.getFullYear();
        const month = (currentDate.getMonth() + 1).toString().padStart(2, '0');
        const day = currentDate.getDate().toString().padStart(2, '0');
        const date_time = `${year}-${month}-${day}`;
        let date_time2 = await redis.get(`Yunzai:meiridaka3qn:${e.user_id}_dakapro`);date_time2 = JSON.parse(date_time2);
        const zhi = await redis.get(`Yunzai:meiridakazhi:${e.user_id}_dakapro`);
        let qian = await redis.get(`Yunzai:meiridakaqian:${e.user_id}_dakapro`);qian = JSON.parse(qian);
        if (date_time === date_time2) {
            let msg = [
                segment.at(e.user_id),
                `\n你今天已经抽过签了，我来帮你找找看……\n签是：【`+qian+`】\n可别再忘掉哦~`
            ]
            e.reply(msg)
            return true;
          }
        const url = `http://shanhe.kim/api/za/chouq.php`
        let res = await fetch(url).catch((err) => logger.error(err))
        res = await res.json()
        let msg = [
            segment.at(e.user_id),
            `\n让我看看……\n你抽到的签：【${res.data.draw}】\n【签文】${res.data.annotate}\n【宜和忌】${res.data.details}\n【小故事】${res.data.source}`
        ]
        e.reply(`正在给你抽签，稍等哦~`)
        await common.sleep(2000);
        e.reply(msg)
        redis.set(`Yunzai:meiridaka3qn:${e.user_id}_dakapro`, JSON.stringify(date_time));
        redis.set(`Yunzai:meiridakazhi:${e.user_id}_dakapro`, JSON.stringify(res.data.format));
        redis.set(`Yunzai:meiridakaqian:${e.user_id}_dakapro`, JSON.stringify(res.data.draw))
        return true;
    }
}