import { segment } from "icqq";
import plugin from '../../lib/plugins/plugin.js';
import cfg from '../../lib/config/config.js'

//在这里设置黑名单用户QQ号，在黑名单中的用户发送关键词时bot不会提醒。用半角逗号隔开(,)
//示例: const banid = '123456789,123456789,123456789';
const banid = '';
//设置是否通知包含关键词的卡片消息（合并转发消息、分享等）
var isKapian = true;//是为true，否为false
//设置是否通知Q群管家的消息
var isQguanjia = true;//是为true，否为false

/**
 * 作者：千奈千祁(gitee.com/QianNQQ)
 * 博客：blog.moqy.fun
 * 
 * 之前在群聊里看见QQ本身有关键词提醒功能，但是要收费，而且还是每个群的提醒词都是分开收费的
 * 所以就做了一个简单的关键词提醒插件。
 * 只会提醒第一个主人，确保你是第一个主人嗷
 */

export class example2 extends plugin {
    constructor () {
      super({
        name: '关键词提醒',
        dsc: '关键词提醒',
        event: 'message',
        priority: -999999999,
        rule: [
          {
            reg: '千奈|千祁',//在这里修改要提示的关键词，用分号|隔开
            fnc: '关键词提醒'
          }
        ]
      })
    }
    async 关键词提醒(e){
      const now = new Date();
      const hours = String(now.getHours()).padStart(2, '0');
      const minutes = String(now.getMinutes()).padStart(2, '0');
      const seconds = String(now.getSeconds()).padStart(2, '0');
      const times = `${hours}:${minutes}:${seconds}`;//时间格式 时:分:秒
      const banidArrays = banid.split(',');
      const isBanned = banidArrays.some(array => array.includes(e.user_id));
      const message = e.message[0]
      let username = e.nickname
      username = username.replace(/⁧/g, '');
      let msg;
      if (!isKapian) {
        if (message.type != 'text') return false
      }
      if (!isQguanjia) {
        if (e.user_id === 2854196310) return false
      }
      if (isBanned) return false;
      msg = [
        segment.image(`https://p.qlogo.cn/gh/${e.group_id}/${e.group_id}/100`),
        `主人主人，关键词提醒\n时间:【${times}】\n群聊:${e.group_name}(${e.group_id})\n用户:${username}(${e.user_id})\n消息:【${e.msg}】`]
      if (message.type != 'text') {
        msg = [
            segment.image(`https://p.qlogo.cn/gh/${e.group_id}/${e.group_id}/100`),
            `主人主人，关键词提醒\n时间:【${times}】\n群聊:${e.group_name}(${e.group_id})\n用户:${username}(${e.user_id})\n该消息为卡片消息`
        ]
        await Bot.pickUser(cfg.masterQQ[0]).sendMsg(msg)
        Bot.pickUser(cfg.masterQQ[0]).sendMsg(e.message)
        return false;
        //e.msg = `该消息为卡片消息，请前往群聊查看`;
      }
        Bot.pickUser(cfg.masterQQ[0]).sendMsg(msg)
        return false;
    }
}