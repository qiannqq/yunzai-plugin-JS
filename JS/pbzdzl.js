import { segment } from "icqq";
import plugin from '../../lib/plugins/plugin.js';
/**
 * 作者：千奈千祁（gitee.com/QianNQQ)
 * QQ: 2632139786
 * 写来练手的，大佬勿喷
 */

export class fengkong extends plugin{
    constructor(){
        super({
            name: '屏蔽指定指令',
            dsc: '屏蔽指定指令',
            event: 'message',
            priority: 1,//这是优先级，能设多高设多高，优先级可以是负数，我这里懒得改了就是1。
            rule: [
                {
                    reg: '^#?椰奶状态(pro)?$|^#?运行状态$',//把要屏蔽的指令正则写进这里面，这里用椰奶状态举例。用|隔开（|一般情况下在你回车键的上方，反正里回车键不远）
                    fnc: 'zhiling'//执行方法
                },
            ]
        })
    }
    async zhiling(e){
        if (e.isMaster) return false;//判断用户权限是否是主人
        e.reply(`用户权限不足，\n执行该指令的最低权限为：Master`)//回复消息，可以可以自己修改，别把``这两个字符删掉就行，\n是换行（也可以直接注释掉这一行，不作回复）
        return true;
    }
}