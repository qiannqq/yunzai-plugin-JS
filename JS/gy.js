import { segment } from "icqq";
import plugin from '../../lib/plugins/plugin.js';
import puppeteer from "../../lib/puppeteer/puppeteer.js"
import moment from 'moment'
import cfg from '../../lib/config/config.js'
import fs from 'fs'
import path from 'path'
import os from 'os'
import axios from 'axios'

//机器人名字
const botname = `云崽`;
//机器人创建时间 用于计算累计运行天数
var targetDate = new Date('2023-01-01');
//pluginDir
const files = fs.readdirSync('./plugins');
//example
const folderPath1 = './plugins/example';
//username userid
let username
let userid
let user

const folderPath = 'resources/aboutyunzai';
const filePath = 'resources/aboutyunzai/aboutyunzai.html';
const fileUrl = 'https://gitee.com/qiannqq/yunzai-plugin-JS/raw/master/html/aboutyunzai.html';

export class example2 extends plugin {
  constructor() {
    super({
      name: `关于${botname}`,
      dsc: `关于${botname}`,
      event: 'message',
      priority: 5000,
      rule: [
        {
          reg: `^#?关于${botname}$`,
          fnc: 'chushihua'
        }
      ]
    })
  }
  async chushihua(e) {
    if (fs.existsSync(filePath)) {
      logger.mark(`[关于${botname}]html模板检测正常`)
      this.关于(e)
    } else {
      logger.mark(`[关于${botname}]正在初始化html模板`)
      try {
        fs.mkdirSync(folderPath, { recursive: true });
        logger.mark(`[关于${botname}]文件夹创建成功`);
      } catch (err) {
        e.reply(`[关于${botname}]初始化：创建文件夹时出错:` + err);
      }
      try {
        fs.writeFileSync(filePath, '', 'utf-8');
        logger.mark(`[关于${botname}]html模板创建成功`);
      } catch (err) {
        e.reply(`[关于${botname}]初始化：创建html模板时出错:` + err);
      }
      axios.get(fileUrl)
        .then((response) => {
          fs.writeFile(filePath, response.data, (err) => {
            if (err) {
              logger.error('写入文件出错:', err);
              e.reply(`[关于${botname}]初始化html模板时出现错误\n` + err)
            } else {
              logger.mark(`[关于${botname}]html模板已成功写入:`, filePath);
              this.关于(e)
            }
          })
        })
      return true;
    }
  }
  async 关于(e) {
    let runTime = moment().diff(moment.unix(Bot.stat.start_time), 'seconds')
    let Day = Math.floor(runTime / 3600 / 24)
    let Hour = Math.floor((runTime / 3600) % 24)
    let Min = Math.floor((runTime / 60) % 60)
    if (Day > 0) {
      runTime = `${Day}天${Hour}小时${Min}分钟`
    } else {
      runTime = `${Hour}小时${Min}分钟`
    }
    var today = new Date();
    var timeDiff = Math.floor((today - targetDate) / (1000 * 60 * 60 * 24));
    const pluginsnb = files.filter(file => fs.statSync(`./plugins/${file}`).isDirectory()).length - 4;
    const files1 = fs.readdirSync(folderPath1);
    const jsFiles = files1.filter(file => path.extname(file).toLowerCase() === '.js');
    const jsnb = jsFiles.length
    const system = os.platform();
    if(typeof e.group_id == `undefined`){
      user = `用户`
      username = e.nickname
      userid = e.user_id
    } else {
      user = `本群`
      username = e.group_name
      userid = e.group_id
    }
    /**logger.mark(`插件数量：${pluginsnb}`)
    logger.mark(`JS数量：${jsnb1}`)
    logger.mark(`平台：${system}`)**/
    image(e, 'aboutyunzai', 'aboutyunzai', {
      botname,
      system,
      jsnb,
      pluginsnb,
      botimf: cfg.package.name,
      botversion: cfg.package.version,
      masterQQ: cfg.masterQQ[0],
      runTime,
      timeDiff,
      username,
      userid,
      user,
    });
    return true;
  }

}
async function image(e, flie, name, obj) {
  let data = {
    quality: 100,
    tplFile: `./resources/aboutyunzai/${flie}.html`,
    ...obj
  }
  let img = puppeteer.screenshot(name, {
    ...data,
  })
  e.reply(img)
}