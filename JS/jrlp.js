/**
 * 作者：千奈千祁(2632139786)
 * Gitee主页：Gitee.com/QianNQQ
 * Github主页：Github.com/QianNQQ
 * 
 * 该插件所有版本发布于 该仓库(https://gitee.com/qiannqq/yunzai-plugin-JS)
 * 本插件及该仓库的所有插件均遵循 GPL3.0 开源协议
 * 
 * 请勿使用本插件进行盈利等商业活动行为
 */


// 是否从最近活跃的群友中抽取
let from_active = false

/**
 * 最近活跃的抽取机制
 * 
 * 该插件将会尝试获取最近的最多40条消息，从这40条消息的发送者中随机出一个作为今日老婆
 * 因此，截止指令发送时，往上40条消息，越活跃的群友被抽到的概率越大
 * (我故意没有去除QQ群管家、机器人自己、和指令发送者，就是为了增加点恶趣味 awa)
 */
/**
 * 完全随机的抽取机制
 * 
 * 顾名思义，该插件将会尝试获取完整的群成员列表，并从群成员列表中随机抽一个出来
 * 该机制完全不受活跃程度影响，被抽到的概率完全公平随机
 */

export class example extends plugin {
    constructor (){
        super({
            name: '随机老婆',
            dsc: '随机老婆',
            event: 'message',
            priority: 10,
            rule: [
                {
                    reg: "^#?今日老婆$",
                    fnc: 'CHOULP',
                }
            ]
        })
    };
    async CHOULP(e) {
        const currentDate = new Date();
        const year = currentDate.getFullYear();
        const month = (currentDate.getMonth() + 1).toString().padStart(2, '0');
        const day = currentDate.getDate().toString().padStart(2, '0');
        const date_time = `${year}-${month}-${day}`; //获取今天的日期
        let date_time2 = await redis.get(`Yunzai:choulpriqi:${e.user_id}_clp`);date_time2 = JSON.parse(date_time2); //获取用户上一次抽cp的日期
        let data_cptime = await redis.get(`Yunzai:dateriqi:${e.user_id}_clp`);data_cptime = JSON.parse(data_cptime)//获取用户
        if(date_time === data_cptime){
            let data_cpname = await redis.get(`Yunzai:choulpqq:${e.user_id}_clp`);data_cpname = JSON.parse(data_cpname)
            let data_cpqq = await redis.get(`Yunzai:choulpname${e.user_id}_clp`);data_cpqq = JSON.parse(data_cpqq)
            let msg = [
                segment.at(e.user_id),
                `\n你今天已经被她娶走了哦~`,
                segment.image(`https://q1.qlogo.cn/g?b=qq&s=0&nk=${data_cpqq}`),
                `【${data_cpname}】(${data_cpqq})\n乖乖的待在她身边不要乱跑哦~`
            ]
            await e.reply(msg)
            return;
        }
        if(date_time === date_time2){//判断日期
            let msg = [`你今天已经有老婆了，还想娶小妾啊？爪巴`]
            await e.reply(msg)
            return;
        }
        //随机一位倒霉蛋群友
        if(!from_active) {
            let mmap = await e.group.getMemberMap();
            let arrMember = Array.from(mmap.values());
            var randomWife = arrMember[Math.floor(Math.random() * arrMember.length)];
        } else {
            let groupHistoryChat = await e.group.getChatHistory(0, 20)
            if(groupHistoryChat.length <= 20) {
                 groupHistoryChat.push(await e.group.getChatHistory(groupHistoryChat[0].seq, 20))
            }
            let GroupUserList = []
            for (let item of groupHistoryChat) {
                if(item && item.sender && item.sender.user_id && item.sender.nickname) {
                    GroupUserList.push(item.sender)
                }
            }
            var randomWife = GroupUserList[Math.floor(Math.random() * GroupUserList.length)];
        }
        //发送老婆
        let msg = [
            segment.at(e.user_id),
            "\n你今天的群CP是",
            segment.image(`https://q1.qlogo.cn/g?b=qq&s=0&nk=${randomWife.user_id}`),
            `【${randomWife.nickname}】(${randomWife.user_id})\n看好她哦，别让她乱跑~`
        ];
        await e.reply(msg);//发送完成
        redis.set(`Yunzai:choulpriqi:${e.user_id}_clp`, JSON.stringify(date_time));//把今天的日期写进去
        redis.set(`Yunzai:choulpcpname:${e.user_id}_clp`, JSON.stringify(randomWife.nickname))//把CP写进去
        redis.set(`Yunzai:choulpcpqq:${e.user_id}_clp`, JSON.stringify(randomWife.user_id))//cp的qq号
        redis.set(`Yunzai:dateriqi:${randomWife.user_id}_clp`, JSON.stringify(date_time))
        redis.set(`Yunzai:choulpname${randomWife.user_id}_clp`, JSON.stringify(e.user_id))
        redis.set(`Yunzai:choulpqq:${randomWife.user_id}_clp`, JSON.stringify(e.nickname))
        return true;
    }
}