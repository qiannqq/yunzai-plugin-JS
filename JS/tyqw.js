import fetch from "node-fetch"
import fs from 'fs'

/**
 * 作者：千奈千祁(2632139786)
 * Gitee主页：Gitee.com/QianNQQ
 * Github主页：Github.com/QianNQQ
 * 
 * 该插件所有版本发布于 该仓库(https://gitee.com/qiannqq/yunzai-plugin-JS)
 * 本插件及该仓库的所有插件均遵循 GPL3.0 开源协议
 * 
 * 请勿使用本插件进行盈利等商业活动行为
 */

let Character_setting = `./plugins/example/Character_setting.txt` //角色设定文件路径位置
let cs_switch = false //是否使用自设定 (非常吃token！！)
let tongyi_sk = `` //通义千问stoken
let tongyi_url = `https://dashscope.aliyuncs.com/api/v1/services/aigc/text-generation/generation` //通义千问API
let tongyi_model = `qwen-plus` //模型名称

/**
 * 关于模型名称
 * 
 *  - 每个模型都可以领免费额度，并不是领了一个就不能领另外一个了，放心选
 * 
 * qwen-turbo 开通后有200万tk的免费额度，有效期180天 （0.008元/1000 tokens）
 * qwen-plus 开通后有100万tk的免费额度，有效期180天 （0.02元/1000 tokens）
 * qwen-max 开通后有100万tk的免费额度，有效期180天 （0.12元/1000 tokens）
 * 
 * 
 *  - 本JS只支持调用以下模型 具体以通义千问接口支持为准 https://help.aliyun.com/zh/dashscope/developer-reference/api-details?spm=a2c4g.11186623.0.i44#f7cf331c06b49
 * qwen-turbo、qwen-plus、qwen-max、qwen-max-1201、qwen-max-longcontext
 * ...
 * 更多模型的免费额度可以在 https://dashscope.console.aliyun.com/billing 查看
 */

export class example2 extends plugin {
    constructor () {
      super({
        name: '通义千问',
        dsc: '通义千问',
        event: 'message',
        priority: -500,
        rule: [
          {
            reg: '^#chat(.*)$',
            fnc: 'chat'
          },
          {
            reg: '^#结束对话$',
            fnc: "endchat"
          }
        ]
      })
    }
    async endchat(e) {
        if(!fs.existsSync(`./data/${e.user_id}_TYQW.json`)) {
            await e.reply([`当前没有开启对话哦`])
            return true
        }
        fs.unlinkSync(`./data/${e.user_id}_TYQW.json`)
        await e.reply([`对话已结束，发送#chat+内容，可开启新的对话`])
        return true
    }
    async chat(e){
        let messages = []
        try {
            messages = JSON.parse(fs.readFileSync(`./data/${e.user_id}_TYQW.json`, `utf-8`))
        } catch {
            messages = []
        }
        if(messages.length == 0 && cs_switch && fs.existsSync(Character_setting)) {
            messages.push({
                role: 'system',
                content: fs.readFileSync(Character_setting, `utf-8`)
            },{
                role: 'user',
                content: e.msg.replace(/#chat/g, ``)
            })
        } else {
            messages.push({
                role: 'user',
                content: e.msg.replace(/#chat/g, ``)
            })
        }
        let options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                "Authorization": `Bearer ${tongyi_sk}`
            },
            body: JSON.stringify({
                model: tongyi_model,
                input: {
                    messages
                }
            })
        }
        let huifu
        try {
            huifu = await fetch(tongyi_url, options)
            huifu = await huifu.json()
        } catch {
            await e.reply([`请求API时出错！请检查网络设置和API地址设置`])
            return true
        }
        if(huifu.code) {
            await e.reply([`出现错误！\n${huifu.code}\n${huifu.message}`])
            return true
        }
        messages.push( {role: "assistant", content: huifu.output.text} )
        fs.writeFileSync(`./data/${e.user_id}_TYQW.json`, JSON.stringify(messages, null, 3), `utf-8`)
        await e.reply([huifu.output.text], true)
        return true
    }
}