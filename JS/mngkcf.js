import fs from 'fs';
import axios from 'axios';
import puppeteer from "../../lib/puppeteer/puppeteer.js"

const folderPath = 'resources/chafen';
const filePath = 'resources/chafen/chafen.html';
const fileUrl = 'https://gitee.com/qiannqq/yunzai-plugin-JS/raw/master/html/chafen.html';

/**
 * 作者：千奈千祁
 * gitee：gitee.com/QianNQQ
 * QQ：2632139786
 * 博客：blog.moqy.fun
 */

export class example2 extends plugin {
    constructor () {
      super({
        name: '高考查分',//插件名
        dsc: '模拟高考查分',//插件描述
        event: 'message',
        priority: 5000,
        rule: [
          {
            reg: '^#?查分$',
            fnc: '查分'
          }
        ]
      })
    }
    async 查分(e){
      if (fs.existsSync(filePath)) {
        logger.mark(`[高考查分]初始化检测正常`)
        this.chafen(e)
      } else {
        logger.mark(`正在初始化`)
        e.reply(`正在初始化……`)
        try {
          fs.mkdirSync(folderPath, { recursive: true });
          logger.mark('文件夹创建成功');
        } catch (err) {
          e.reply('创建文件夹时出错:' + err);
        }
        try {
          fs.writeFileSync(filePath, '', 'utf-8');
          logger.mark('文件创建成功');
        } catch (err) {
          e.reply('创建文件时出错:' + err);
        }
        axios.get(fileUrl)
        .then((response) => {
          fs.writeFile(filePath, response.data, (err) => {
            if (err) {
              logger.error('写入文件出错:', err);
              e.reply(`初始化时出现错误\n` + err)
            } else {
              logger.mark('文件已成功写入:', filePath);
              e.reply(`初始化完成`)
              this.chafen(e)
            }
          })
        })
        return true;
      }
    }
    async chafen(e){
        let zhi1 = await redis.get(`Yunzai:chafen${e.user_id}`);zhi1 = JSON.parse(zhi1);
        if (zhi1 === 8) {
          let yuwen = await redis.get(`Yunzai:chafen${e.user_id}_yuwen`);yuwen = JSON.parse(yuwen);
          let shuxue = await redis.get(`Yunzai:chafen${e.user_id}_shuxue`);shuxue = JSON.parse(shuxue);
          let yingyu = await redis.get(`Yunzai:chafen${e.user_id}_yingyu`);yingyu = JSON.parse(yingyu);
          let wuli = await redis.get(`Yunzai:chafen${e.user_id}_wuli`);wuli = JSON.parse(wuli);
          let zhengzhi = await redis.get(`Yunzai:chafen${e.user_id}_zhengzhi`);zhengzhi = JSON.parse(zhengzhi);
          let huaxue = await redis.get(`Yunzai:chafen${e.user_id}_huaxue`);huaxue = JSON.parse(huaxue);
          let zongfen = await redis.get(`Yunzai:chafen${e.user_id}_zongfen`);zongfen = JSON.parse(zongfen);
          const user_id = e.user_id;
          const user_name = e.nickname;
          image(e, 'chafen', 'chafen',{
            user_id,
            user_name,
            yuwen,
            shuxue,
            yingyu,
            wuli,
            zhengzhi,
            huaxue,
            zongfen,
          });
            /**let msg = [
                segment.at(e.user_id),
                `\n你已经查过分了\n你的分数是：\n语文：`+yuwen1+`\n数学：`+shuxue1+`\n英语：`+yingyu+`\n物理：`+wuli+`\n政治：`+zhengzhi+`\n化学：`+huaxue+`
                总分：`+zongfen+``]
            e.reply(msg)**/
            return true;
          }
        let zhi = 8;
        redis.set(`Yunzai:chafen${e.user_id}`, JSON.stringify(zhi));
        const yuwen = Math.floor(Math.random() * 111) + 40;
        const shuxue = Math.floor(Math.random() * 111) + 40;
        const yingyu = Math.floor(Math.random() * 111) + 40;
        const wuli = Math.floor(Math.random() * 71) + 30;
        const zhengzhi = Math.floor(Math.random() * 71) + 30;
        const huaxue = Math.floor(Math.random() * 71) + 30;
        const zongfen = yuwen + shuxue + yingyu + wuli + zhengzhi + huaxue;
        const user_name = e.nickname;
        const user_id = e.user_id;
        image(e, 'chafen', 'chafen',{
          user_id,
          user_name,
          yuwen,
          shuxue,
          yingyu,
          wuli,
          zhengzhi,
          huaxue,
          zongfen,
        });
        /**let msg = [
            segment.at(e.user_id),
            `\n你的高考成绩出来了！！\n语文：`+yuwen+`\n数学：`+shuxue+`\n英语：`+yingyu+`\n物理：`+wuli+`\n政治：`+zhengzhi+`\n化学：`+huaxue+`
        总分：`+zongfen+``]**/
        redis.set(`Yunzai:chafen${e.user_id}_yuwen`, JSON.stringify(yuwen));//语文
        redis.set(`Yunzai:chafen${e.user_id}_shuxue`, JSON.stringify(shuxue));//数学
        redis.set(`Yunzai:chafen${e.user_id}_yingyu`, JSON.stringify(yingyu));//英语
        redis.set(`Yunzai:chafen${e.user_id}_wuli`, JSON.stringify(wuli));//物理
        redis.set(`Yunzai:chafen${e.user_id}_zhengzhi`, JSON.stringify(zhengzhi));//政治
        redis.set(`Yunzai:chafen${e.user_id}_huaxue`, JSON.stringify(huaxue));//化学
        redis.set(`Yunzai:chafen${e.user_id}_zongfen`, JSON.stringify(zongfen));//总分
        //e.reply(msg)
        return true;
    }
}
async function  image(e, flie, name, obj){
  let data = {
    quality: 100,
    tplFile: `./resources/chafen/${flie}.html`,
    ...obj
  }
  let img = puppeteer.screenshot(name,{
    ...data,
  })
  e.reply(img)
}