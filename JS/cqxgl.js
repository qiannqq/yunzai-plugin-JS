import plugin from '../../lib/plugins/plugin.js';
import {promises as fs} from 'fs';
import { promisify } from 'util';
import { readFile, writeFile } from 'fs';
const readFileAsync = promisify(readFile);
const writeFileAsync = promisify(writeFile);


//配置文件路径
const userblack = 'resources/userblack/userblack.txt'
const userconfig = 'resources/userblack/userconfig.txt'
const userbanGN = 'resources/userblack/userbanGN.txt'

/**
 * 作者：千奈千祁
 * Gitee主页：Gitee.com/QianNQQ
 * 支持特定群聊ban用户和unban用户
 * 支持特定群聊禁用和解禁指令
 * 支持在群聊内添加次权限用户
 * 
 * 更多管理功能还在编写……
 */

export class example2 extends plugin {
    constructor() {
        super({
            name: '次权限管理',
            dsc: '次权限管理',
            event: 'message',
            priority: -999999999999999999999999999999999999999999,
            rule: [
                {
                    reg: /^#云崽ban\s*([0-9]+)\s*$/,
                    fnc: '次权限ban'
                },
                {
                    reg: /^#云崽unban\s*([0-9]+)\s*$/,
                    fnc: '次权限unban'
                },
                {
                    reg: /^#新增权限用户\s*([0-9]+)\s*$/,
                    fnc: '新增权限用户',
                    permission: 'master'
                },
                {
                    reg: /^#移除权限用户\s*([0-9]+)\s*$/,
                    fnc: '移除权限用户',
                    permission: 'master'
                },
                {
                    reg: /^#云崽禁用\s*([\u4e00-\u9fa5\dA-Za-z]+)\s*$/,
                    fnc: '次权限禁用'
                },
                {
                    reg: /^#云崽解禁\s*([\u4e00-\u9fa5\dA-Za-z]+)\s*$/,
                    fnc: '次权限解禁'
                },
                {
                    reg: '(.*)',
                    fnc: '黑名单检测',
                    log: false
                }
            ]
        })
    }
    async 次权限解禁(e){
      //是否群聊
      if(!e.isGroup){
        e.reply(`请在群聊内操作~`)
        return true;
      }
      //获取关键词
      const gongneng = e.raw_message.match(/^#云崽解禁\s*([\u4e00-\u9fa5\dA-Za-z]+)\s*$/)[1]
      //duquFile筛选userconfig中符合条件的数据
      let CfgID = await duquFile(userconfig, e, `user`, `false`)
      //判断CfgID是否为空，为空则用户不符合条件
      if (CfgID.length == 0) {
        logger.mark(`[次权限管理]运行失败，用户(${e.user_id})权限不足`);
        return true;
      }
      //duquFile筛选userbanGN中是否存在该关键词
      let CfgGN = await duquFile(userbanGN, e, gongneng, `false`)
      logger.mark(CfgGN)
      //判断CfgGN是否为空，为空则关键词不存在
      if(CfgGN.length == 0){
        await e.reply(`包含关键词“${gongneng}”的指令未被禁用`)
        logger.mark(`[次权限管理]运行失败，包含“${gongneng}”的指令未被禁用`)
        return true;
      }
      //构建变量
      const banGn = `-${gongneng}；${e.group_id}`
      logger.mark(banGn)
      //shanchu删除userbanGN中的banGn内容
      await shanchu(userbanGN, banGn)
      await e.reply(`包含关键词“${gongneng}”的指令已成功解禁`)
      return true;
    }
    async 次权限禁用(e){
      //是否群聊
      if(!e.isGroup){
        e.reply(`请在群聊内操作~`)
        return true;
      }
      //获取关键词
      const gongneng = e.raw_message.match(/^#云崽禁用\s*([\u4e00-\u9fa5\dA-Za-z]+)\s*$/)[1]
      //duquFile筛选userconfig中符合条件的数据
      let CfgID = await duquFile(userconfig, e, `user`, `false`)
      //判断CfgID是否为空，为空则用户不符合条件 
      if(CfgID.length == 0){
        logger.mark(`[次权限管理]运行失败，用户(${e.user_id})权限不足`);
        return true;
      }
      //duquFile筛选userbanGN中是否存在关键词
      let CfgGN = await duquFile(userbanGN, e, gongneng, `false`)
      //判断CfgGN是否为空，不为空则相同关键词已存在
      if(CfgGN.length > 0){
        await e.reply(`包含关键词“${gongneng}”的指令已经被禁用了，请勿重复禁用`)
        logger.mark(`[次权限管理]运行失败，包含“${gongneng}”的指令已经被禁用`)
        return true;
      }
      //构建变量
      const banGN = `-${gongneng}；${e.group_id}\n`
      //fs.appendFile将banGN添加至userbanGN
      await fs.appendFile(userbanGN, banGN, `utf-8`)
      await e.reply(`包含关键词"${gongneng}"的指令已无法在${e.group_id}使用了`);
      return true;
    }
    async 移除权限用户(e){
      //是否群聊
      if(!e.isGroup){
        e.reply(`请在群聊内删除~`)
        return true;
      }
      //获取ID
      const ycqx = e.raw_message.match(/^#移除权限用户\s*([0-9]+)\s*$/)[1]
      //duquFile筛选userconfig中符合条件的数据
      let ycqxPD = await duquFile(userconfig, e, ycqx, `false`)
      //判断ycqxPD是否为空，为空则ID不具有e.group_id权限
      if(ycqxPD.length = 0){
        e.reply(`用户${ycqx}并不具有${e.group_id}的次权限`)
        return true;
      }
      //构建变量
      let quanxianid = [`-${ycqx}；${e.group_id}`]
      //shanchu移除userconfig中的quanxianid内容
      await shanchu(userconfig, quanxianid)
      await e.reply(`已移除用户${ycqx}在${e.group_id}的次权限`)
      return true;
    }
    async 新增权限用户(e){
      //是否群聊
      if(!e.isGroup) {
        e.reply(`请在群聊内添加~`)
        return true;
      }
      //获取ID
      const quanxianid1 = e.raw_message.match(/^#新增权限用户\s*([0-9]+)\s*$/)[1]
      //duquFile筛选userconfig中符合条件的数据
      let quanxianPD = await duquFile(userconfig, e, quanxianid1, `false`)
      //判断quanxianPD是否为空，不为空则ID已具有e.group_id的权限
      if(quanxianPD.length > 0){
        e.reply(`用户${quanxianid1}已拥有该群的次权限，请勿重复添加`)
        return true;
      }
      //构建变量
      let quanxianid = [`-${quanxianid1}；${e.group_id}\n`]
      //fs.appendFile将quanxianid添加至userconfig
      await fs.appendFile(userconfig, quanxianid, 'utf-8')
      e.reply(`已赋予用户${quanxianid1}的群${e.group_id}次权限了~`)
      return true;
    }
    async 次权限unban(e) {
      //是否群聊
      if(!e.isGroup) {
        e.reply(`请在群聊内添加~`)
        return true;
      }
      //获取ID
      const unbanid = e.raw_message.match(/^#云崽unban\s*([0-9]+)\s*$/)[1]
      //duquFile筛选userconfig中符合条件的数据
      let CfgID = await duquFile(userconfig, e, `user`, `false`)
      //判断CfgID是否为空，为空则e.user_id不具有e.group_id的权限
      if(CfgID.length == 0){
        logger.mark(`[次权限管理]运行失败，用户(${e.user_id})权限不足`)
        return true;
      }
      //duquFile筛选userblack中符合条件的数据
      let CfgGN = await duquFile(userblack, e, unbanid, 'false')
      //判断CfgGN是否为空，为空则ID未被封禁
      if(CfgGN.length == 0){
        await e.reply(`用户${unbanid}未被封禁`)
        return true;
      }
      //构建变量
      const banGn = `-${unbanid}；${e.group_id}`
      //shanchu移除userblack中的banGn内容
      await shanchu(userblack, banGn)
      await e.reply(`用户${unbanid}可以在${e.group_id}使用云崽了`)
      return true;
    }
    async 次权限ban(e) {
      //是否群聊
      if(!e.isGroup) {
        e.reply(`请在群聊内添加~`)
        return true;
      }
      //获取ID
      const banid = e.raw_message.match(/^#云崽ban\s*([0-9]+)\s*$/)[1];
      //duquFile筛选userconfig中符合条件的数据
      let CfgID = await duquFile(userconfig, e, `user`, `false`)
      //判断CfgID是否为空，为空则e.user_id不具有e.group_id的权限
      if(CfgID.length == 0){
        logger.mark(`[次权限管理]运行失败，用户(${e.user_id})权限不足`)
        return true;
      }
      //duquFile筛选userblack中符合条件的数据
      let CfgGN = await duquFile(userblack, e, banid, `false`)
      //判断CfgGN是否为空，不为空则ID已经被封禁
      if(CfgGN.length > 0){
        await e.reply(`用户${banid}已经被ban了，请勿重复ban`)
        return true;
      }
      //构建变量
      const banGN = `-${banid}；${e.group_id}\n`
      //fs.appendFile将banGN内容添加至userblack
      await fs.appendFile(userblack, banGN, `utf-8`)
      await e.reply(`用户${banid}已无法在${e.group_id}使用云崽了`)
    }
    async 黑名单检测(e) {
      //是否群聊
      if(!e.isGroup) return false
      //duquFile筛选发送消息的用户是否符合条件
      let blackID = await duquFile(userblack, e, `user`, `false`)
      //判断blackID是否为空，不为空则该用户符合条件，返回true
      if(blackID.length > 0){
        e.msg = `1`
        logger.mark(`用户屏蔽`)
        return true;
      }
      //判断e.msg是否为空，为空则非文本消息，
      let GNmsg;
      if(e.msg == undefined){
        return false;
        //判断e.msg是否为#喵喵WIKI，如果是则获取e.message消息进行判断
      } else {
        for (let i = 0; i < e.message.length; i++) {
          const msg = e.message[i];
          if (msg.type === 'text') {
            GNmsg = msg.text;
          }
        }
      }
      //duquFile筛选发送的消息是否含有关键词
      let blackMsg = await duquFile(userbanGN, e, GNmsg, `true`)
      //判断blackMsg是否为空，不为空则该消息符合条件，返回true。为空则该消息不符合条件，返回false
      if(blackMsg.length > 0){
        e.msg = `1`
        logger.mark(`指令屏蔽`)
        return true;
      } else {
        return false;
      }
    }
}

async function duquFile(file, e, ZDY, jc){
  //声明变量
  let ZDYnr;
  //判断ZDY是否为user，是则ZDYnr设为e.user_id，否则ZDYnr设为ZDY
  if(ZDY == `user`){
    ZDYnr = e.user_id;
  } else {
    ZDYnr = ZDY;
  }
  //获取file中的内容
  let data = await fs.readFile(file, 'utf-8');
  //处理内容，按格式分割内容
  const lines = data.split('-');
  let CfgID = [];
  lines.forEach((line) => {
    line= line.slice(0, -1);
    const parts = line.split('；');
    const id = parts[0];
    const groupid = parts[1];
    //判断ID是否符合ZDYnr，符合则进入下一个判断
    if(id == ZDYnr){
      //判断groupid是否符合e.group_id，符合则push
      if(groupid == e.group_id){
        if(e.group_id == groupid) CfgID.push(`-${id}；${groupid}`);
      }
    //ID不符合ZDYnr，则判断jc是否符合true
    } else if(jc == `true`){
      //判断groupid是否符合undefined，筛除幽灵内容
      if(groupid != undefined){
        //判断ZDYnr是否含有内容ID，符合则push
      if(ZDYnr.includes(id)){
        CfgID.push(`-${id}；${groupid}`)
      }
    }}
  });
  //返回处理完成的数据
  return CfgID
}

async function shanchu(filePath, shuju1) {
  try {
    logger.mark('OK');
    const data = await readFileAsync(filePath, 'utf8');
    const regex = new RegExp(shuju1 + '\\r?\\n?', 'g');
    const updatedData = data.replace(regex, '');
    await writeFileAsync(filePath, updatedData, 'utf8');
    logger.mark('删除成功');
  } catch (error) {
    logger.error(error);
    e.reply(`发生错误: ${error}`);
  }
}