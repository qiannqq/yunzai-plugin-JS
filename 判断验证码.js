import { segment } from "icqq";
import plugin from '../../lib/plugins/plugin.js';
//import { trim } from "lodash";
//import cfg from '../../lib/config/config.js'
/**
 * 作者：千奈千祁
 * 这个验证码不能和其它人同时验证
 * 这玩意不是直接扔到example的，需要把这一段代码复制到插件包里你需要加验证码的功能里。
 */


let GayCD = {};//这个也得加

export class example extends plugin{
    constructor(){//这一段没必要加
        super({
            name: '后台验证码',
            dsc: '日志验证码',
            event: 'message',
            priority: -9999999,
            rule:[
                {
                    reg: '^#验证码测试$',
                    fnc: 'yanzhengmatest',//执行方式要改
                }
            ]
        })
    }
      
        async yanzhengmatest(e) {//这一大段直接复制过去
          let zhi1 = await redis.get(`Yunzai:ip_yanzm`);zhi1 = JSON.parse(zhi1);
          if (zhi1 === 1) {
            let msg = [`请稍后……`];
            e.reply(msg);
            return;
          }
          console.log("用户命令：", e.msg);
          if (GayCD[e.user_id]) {
            e.reply("请60秒后再重试。");//冷却时间
            return true;
          }
          GayCD[e.user_id] = true;
          GayCD[e.user_id] = setTimeout(() => {
            if (GayCD[e.user_id]) {
              delete GayCD[e.user_id];
            }
          }, 60000);//冷却时间在这里改，单位毫秒
          let zhi = 1;
          redis.set(`Yunzai:ip_yanzm`, JSON.stringify(zhi)); //防止重复使用该功能
          const yanzm = Math.floor(100000 + Math.random() * 900000);//随机6位数字
          logger.mark(`验证码：${yanzm}`);//往后台日志发验证码
          let msg = [`请前往日志查看验证码。`];
          e.reply(msg);
          this.setContext('yanzhengma')//上下文，把你要加验证的功能改一下
          redis.set(`Yunzai:yanzm_yanzm`, JSON.stringify(yanzm));
          redis.set(`Yunzai:yanzm_times:${this.e.user_id}`, '3')
          return true;
        }
        async yanzhengma(){//判断验证码的地方
            let yanzm = await redis.get(`Yunzai:yanzm_yanzm`);yanzm = JSON.parse(yanzm);
            if (this.e.msg == yanzm){
                await this.reply("验证码判断成功！")//判断成功，然后把要执行的功能套进去
                let zhi2 = 0;//这一段别删，不然下一次验证直接卡死
                redis.set(`Yunzai:ip_yanzm`, JSON.stringify(zhi2));//这一段别删，不然下一次验证直接卡死
                //把加验证的功能套到这里(注意前面要套个this)
            }else{
                let time = redis.get(`Yunzai:yanzm_times:${this.e.user_id}`)
                if (time != 0){
                redis.set(`Yunzai:yanzm_times:${this.e.user_id}`, time - 1)
                await this.reply(["验证码判断失败。你还有", time - 1,'次机会'])//判断失败，结束指令运行
                }else{
                let zhi2 = 0;
                redis.set(`Yunzai:ip_yanzm`, JSON.stringify(zhi2));
                //直接结束运行
                this.finish("yanzhengma") 
                }
            }
        }}