import plugin from '../../lib/plugins/plugin.js'
import {segment} from "icqq";
const help = 'https://s1.ax1x.com/2023/06/28/pCdAy0P.jpg'

const banid = '';


//作者：千奈千祁(QQ:2632139786) https://gitee.com/qiannqq
//接口https://free.wqwlkj.cn/


export class example extends plugin {
	constructor() {
		super({
			/** 功能名称 */
			name: '表情合成（@千奈千祁）',
			/** 功能描述 */
			dsc: '表情合成（@千奈千祁）',
			event: 'message',
			priority: 2,
			rule: [{
					reg: '^表情合成帮助?$',
					fnc: 'help'
				},
				{
					reg: '^摸(.*)$',
					fnc: 'mo'
				},
				{
					reg: '^教我写代码(.*)$',
					fnc: "jwxdm"
				},
				{
					reg: '^差评(.*)$',
					fnc: "cp"
				},
				{
					reg: '^好评(.*)$',
					fnc: "hp"
				},
				{
					reg: '^没钱难办事(.*)$',
					fnc: "mqnbs"
				},
				{
					reg: '^抱着哭(.*)$',
					fnc: "bao"
				},
				{
					reg: '^羡慕(.*)$',
					fnc: "xianmu"
				},
				{
					reg: '^拍死你憨批(.*)$',
					fnc: "pai"
				},
				{
					reg: '^应援(.*)$',
					fnc: "tui"
				},
				{
					reg: '^色(.*)$',
					fnc: "se"
				},
				{
					reg: '^爬(.*)$',
					fnc: "pa"
				},
				{
					reg: '^纳西妲抱(.*)$',
					fnc: "纳西妲抱"
				},
				{
					reg: '^订婚证(.*)$',
					fnc: "订婚证"
				},
				{
					reg: '^想(.*)$',
					fnc: "想"
				},
				{
					reg: '^偷瞄(.*)$',
					fnc: "偷瞄"
				},
				{
					reg: '^想要钱(.*)$',
					fnc: "想要钱"
				},
				{
					reg: '^轻轻敲醒(.*)$',
					fnc: "敲醒"
				},
				{
					reg: '^求婚(.*)$',
					fnc: "求婚"
				},
				{
					reg: '^听音乐(.*)$',
					fnc: "听音乐"
				},
			]
		})
	}




	async help(e) {
		let msg
		msg = [
			 help ? segment.image(help) : "",
			]
		await e.reply(msg)
		return true;
	}

	async mo(e) {
		if (e.message[1].type == 'at') {
			e.reply(segment.image(`http://free.wqwlkj.cn/wqwlapi/mo.php?qq=${e.at}`))
		}
		return true;
	}

	async jwxdm(e) {
		if (e.message[1].type == 'at') {
			e.reply(segment.image(`http://free.wqwlkj.cn/wqwlapi/jwxdm.php?qq=${e.at}`))
		}
		return true;
	}

	async cp(e) {
		if (e.message[1].type == 'at') {
			e.reply(segment.image(`http://free.wqwlkj.cn/wqwlapi/cp.php?qq=${e.at}`))
		}
		return true;
	}

	async hp(e) {
		if (e.message[1].type == 'at') {
			e.reply(segment.image(`http://free.wqwlkj.cn/wqwlapi/hp.php?qq=${e.at}`))
		}
		return true;
	}

	async mqnbs(e) {
		if (e.message[1].type == 'at') {
			e.reply(segment.image(`http://free.wqwlkj.cn/wqwlapi/ban.php?qq=${e.at}`))
		}
		return true;
	}

	async bao(e) {
		if (e.message[1].type == 'at') {
			e.reply(segment.image(`http://api.caonm.net/api/bzk/index.php?qq=${e.at}`))
		}
		return true;
	}

	async xianmu(e) {
		if (e.message[1].type == 'at') {
			e.reply(segment.image(`http://free.wqwlkj.cn/wqwlapi/xianmu.php?qq=${e.at}`))
		}
		return true;
	}

	async pai(e) {
		if (e.message[1].type == 'at') {
			e.reply(segment.image(`http://free.wqwlkj.cn/wqwlapi/pai.php?qq=${e.at}`))
		}
		return true;
	}

	async tui(e) {
		if (e.message[1].type == 'at') {
			e.reply(segment.image(`http://free.wqwlkj.cn/wqwlapi/tui.php?qq=${e.at}`))
		}
		return true;
	}

	async se(e) {
		if (e.message[1].type == 'at') {
			e.reply(segment.image(`http://free.wqwlkj.cn/wqwlapi/se.php?qq=${e.at}`))
		}
		return true;
	}

	async pa(e) {
		if (e.message[1].type == 'at') {
			e.reply(segment.image(`http://free.wqwlkj.cn/wqwlapi/pa.php?qq=${e.at}`))
		}
		return true;
	}
	async 纳西妲抱(e) {
		if (e.message[1].type == 'at') {
			e.reply(segment.image(`https://xiaobapi.top/api/xb/api/naxidabao.php?qq=${e.at}`))
		}
		return true;
	}
	async 订婚证(e) {
		if (e.message[1].type == 'at') {
			e.reply(segment.image(`https://xiaobapi.top/api/xb/api/stickers_certificate_4.php?qq=${e.at}`))
		}
		return true;
	}
	async 想(e) {
		if (e.message[1].type == 'at') {
			e.reply(segment.image(`https://xiaobapi.top/api/xb/api/xiang_4.php?qq=${e.at}`))
		}
		return true;
	}
	async 偷瞄(e) {
		if (e.message[1].type == 'at') {
			e.reply(segment.image(`https://xiaobapi.top/api/xb/api/toumiao.php?qq=${e.at}`))
		}
		return true;
	}
	async 想要钱(e) {
		if (e.message[1].type == 'at') {
			e.reply(segment.image(`https://xiaobapi.top/api/xb/api/want_get_money.php?qq=${e.at}`))
		}
		return true;
	}
	async 敲醒(e) {
		if (e.message[1].type == 'at') {
			e.reply(segment.image(`https://xiaobapi.top/api/xb/api/qiao.php?qq=${e.at}`))
		}
		return true;
	}
	async 求婚(e) {
		if (e.message[1].type == 'at') {
			e.reply(segment.image(`https://xiaobapi.top/api/xb/api/qiuhun.php?qq=${e.at}`))
		}
		return true;
	}
	async 听音乐(e) {
		if (e.message[1].type == 'at') {
			e.reply(segment.image(`https://xiaobapi.top/api/xb/api/listen_music.php?qq=${e.at}`))
		}
		return true;
	}


}
