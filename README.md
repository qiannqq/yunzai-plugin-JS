# Yunzai-Bot JS插件库
#### 介绍

闲的没事写来练手的JS插件

#### 目录

- [插件安装器1.0版.js](https://gitee.com/qiannqq/yunzai-plugin-JS#%E6%8F%92%E4%BB%B6%E5%AE%89%E8%A3%85%E5%99%A8js)
- [标准输入.js](https://gitee.com/qiannqq/yunzai-plugin-JS#%E6%A0%87%E5%87%86%E8%BE%93%E5%85%A5js)
- [原神活动到期推送.js](https://gitee.com/qiannqq/yunzai-plugin-JS#%E5%8E%9F%E7%A5%9E%E6%B4%BB%E5%8A%A8%E5%88%B0%E6%9C%9F%E6%8E%A8%E9%80%81js)
- [通义千问.js](https://gitee.com/qiannqq/yunzai-plugin-JS#%E9%80%9A%E4%B9%89%E5%8D%83%E9%97%AEjs)    
- [发言统计.js](https://gitee.com/qiannqq/yunzai-plugin-JS#%E5%8F%91%E8%A8%80%E7%BB%9F%E8%AE%A1js)
- [广播通知.js](https://gitee.com/qiannqq/yunzai-plugin-JS#%E5%B9%BF%E6%92%AD%E9%80%9A%E7%9F%A5js)
- [违规图片管制.js](https://gitee.com/qiannqq/yunzai-plugin-JS#%E8%BF%9D%E8%A7%84%E5%9B%BE%E7%89%87%E7%AE%A1%E5%88%B6js)
- [加群申请自动处理.js](https://gitee.com/qiannqq/yunzai-plugin-JS#%E5%8A%A0%E7%BE%A4%E7%94%B3%E8%AF%B7%E8%87%AA%E5%8A%A8%E5%A4%84%E7%90%86js)
- [次权限管理.js](https://gitee.com/qiannqq/yunzai-plugin-JS#%E6%AC%A1%E6%9D%83%E9%99%90%E7%AE%A1%E7%90%86js)
- [关于.js](https://gitee.com/qiannqq/yunzai-plugin-JS#%E5%85%B3%E4%BA%8Ejs)
- [云崽上下班.js](https://gitee.com/qiannqq/yunzai-plugin-JS#%E4%BA%91%E5%B4%BD%E4%B8%8A%E4%B8%8B%E7%8F%ADjs)
- [关键词提醒.js](https://gitee.com/qiannqq/yunzai-plugin-JS#%E5%85%B3%E9%94%AE%E8%AF%8D%E6%8F%90%E9%86%92js)
- [今日老婆.js](https://gitee.com/qiannqq/yunzai-plugin-JS#%E4%BB%8A%E6%97%A5%E8%80%81%E5%A9%86js)
- [模拟高考查分.js](https://gitee.com/qiannqq/yunzai-plugin-JS#%E6%A8%A1%E6%8B%9F%E9%AB%98%E8%80%83%E6%9F%A5%E5%88%86js)
- [屏蔽指定指令，只有主人才可执行.js](https://gitee.com/qiannqq/yunzai-plugin-JS#%E5%B1%8F%E8%94%BD%E6%8C%87%E5%AE%9A%E6%8C%87%E4%BB%A4%E5%8F%AA%E6%9C%89%E4%B8%BB%E4%BA%BA%E6%89%8D%E5%8F%AF%E6%89%A7%E8%A1%8Cjs)
- [每日打卡.js](https://gitee.com/qiannqq/yunzai-plugin-JS#%E6%AF%8F%E6%97%A5%E6%89%93%E5%8D%A1js)
- [抽签.js](https://gitee.com/qiannqq/yunzai-plugin-JS#%E6%8A%BD%E7%AD%BEjs)

#### 访问量
![访问量](https://profile-counter.glitch.me/yunzai-plugin-JS/count.svg)


---
## 插件安装器1.0版.js
##### 安装
```
curl -o "./plugins/example/插件安装器.js" "https://gitee.com/qiannqq/yunzai-plugin-JS/raw/master/JS/cjazq.js"
```
##### 简介
可以安装插件包的js插件，支持卸载、重装、插件列表、已安装插件。<br>
1.0版本更新了3个功能，分别为`JS管理、插件管理、插件载入`。详情指令请插件【指令以及其作用】<br>
得益于`APUPC(自动解析更新插件列表客户端)`自动化程序，插件列表可以实现自动更新，理论无延迟
##### 指令以及其作用
| 指令        | 作用                  | 权限 |
|-----------|---------------------|----|
| #插件列表     | 查看所有插件              | 主人 |
| #已安装插件    | 查看已安装的插件            | 主人 |
| #安装插件+插件名 | 该指令可在“#插件列表”中获取插件名  | 主人 |
| #卸载插件+插件名 | 该指令可在“#已安装插件”中获取插件名 | 主人 |
| #搜索插件+相关信息 | 该指令会从插件列表中搜索插件名、描述、作者等获取包含“相关信息”的插件 | 主人 |
| #安装JS插件+插件名 | 该指令可在“#插件列表”中获取插件名 | 主人 |
| #卸载JS插件+插件名 | 该指令可在“#已安装插件”中获取插件名 | 主人 |
| #启用插件+插件名 | 启用插件，该指令可在“#已安装插件”中获取插件名 | 主人 |
| #禁用插件+插件名 | 通过禁用插件的index.js使插件不会被载入，该指令可在“#已安装插件”中获取插件名（实验性功能可能会使被禁用插件报错） | 主人 |
| #更新JS插件+插件名 | （完善中...） | 主人 |
| #载入插件 | 在不重启的情况下载入新安装(启用)的插件 | 主人 |



---
## 标准输入.js
##### 安装
```
curl -o "./plugins/example/标准输入.js" "https://gitee.com/qiannqq/yunzai-plugin-JS/raw/master/JS/stdin.js"
```
##### 简介
因Miao-Yunzai本身不支持控制台输入指令，所以就有了标准输入<br>
标准输入允许你在控制台输入任何指令，其权限与主人相同（如果出现非主人权限请在other.yaml里将`55555`添加至masterQQ列表）<br>
（如出现兼容性问题请及时创建issue，我看到后会尽快处理）
##### 指令以及其作用
| 指令 | 作用                              | 权限 |
|----|---------------------------------|----|
| *  | 在控制台输入的消息，将传递给插件处理，例如“#帮助、#状态”等 | *  |



---
## 原神活动到期推送.js
- **该功能已PR到Miao-Yunzai，无需安装此JS，支持星铁。更新Miao-Yunzai后在群里发送`#原神开启到期活动推送`，星铁发送`#星铁开启到期活动推送`**
<!--##### 安装
```
curl -o "./plugins/example/原神活动到期推送.js" "https://gitee.com/qiannqq/yunzai-plugin-JS/raw/master/JS/hdts.js"
```
##### 简介
在指定群推送即将到期关闭的原神活动(星铁活动待支持中... 画大饼.jpg)<br>
**该插件灵感来源[@UCPr](https://gitee.com/UCPr251)**
##### 指令以及其作用
| 指令      | 作用       | 权限 |
|---------|----------|----|
| #手动推送活动 | 手动触发一次推送 | 主人 |
##### 设置项
| 设置项               | 作用                           | 值            |
|-------------------|------------------------------|--------------|
| 推送群列表(pushGroups) | 需要推送即将到期活动的群                 | 数组、数字        |
| 机器人QQ(Botqq)      | 兼容适配器，填机器人QQ，频道和官方群机器人填AppID | 数字           |
| 到期天数阈值(yujing)    | 活动剩余多少天开始推送，默认一天             | 数字           |
| 定时推送时间(pushtime)  | 推送时间，默认每天10点                 | 字符串(cron表达式) |-->




---
## 通义千问.js
##### 安装
```
curl -o "./plugins/example/通义千问.js" "https://gitee.com/qiannqq/yunzai-plugin-JS/raw/master/JS/tyqw.js"
```
##### 简介
对接通义千问API的JS轻量插件，支持上下文、自设定，指定模型等。<br>
可在[此处](https://www.yunzai.chat/tutorial/models/qwen.html)查看通义千问的SK获取方式，领取免费额度，以及模型名称的区别
##### 指令以及其作用
| 指令       | 作用          | 权限  |
|----------|-------------|-----|
| #chat+内容 | 与通义千问对话   | 所有人 |
| #结束对话 | 结束上下文重新开始对话 | 所有人 |
##### 设置项
| 设置项                          | 作用          | 值   |
|------------------------------|-------------|-----|
| 自设定角色文件路径(Character_setting) | 让通义千问扮演某个角色 | 字符串 |
| 是否启用自设定(cs_switch) | 是否启用自设定 | true\false（是或否） |
| 通义千问API的SK(tongyi_sk) | 必须！通义千问的调用令牌 | 字符串 |
| 通义千问API(tongyi_url) | 默认即可 | 字符串 |
| 通义千问调用的模型名称(tongyi_model) | 关于模型名称，详情请看JS内备注 | 字符串 |



---
## 发言统计.js
##### 安装
```
curl -o "./plugins/example/发言统计.js" "https://gitee.com/qiannqq/yunzai-plugin-JS/raw/master/JS/fytj.js"
```
##### 简介
类似于水群统计，可以查看当前群谁话最多最密 `注意：仅插件安装后才会开始统计`
##### 指令以及其作用
| 指令     | 作用            | 权限 |
|--------|---------------|----|
| #发言榜日榜 | 查看当前群今天都是谁在水群 | 用户 |
| #发言榜月榜 | 查看当前群本月都是谁在水群 | 用户 |
##### 设置项
| 设置项         | 作用              | 值  |
|-------------|-----------------|----|
| 榜单上限人数(lul) | 设置榜单最高多少人，默认20人 | 数字 |



---
## 广播通知.js
##### 安装
```
curl -o "./plugins/example/广播通知.js" "https://gitee.com/qiannqq/yunzai-plugin-JS/raw/master/JS/gbtz.js"
```
##### 简介
一个简易的群发插件，不可单选群<br>
支持群发白名单、黑名单、所有群
##### 指令以及其作用
| 指令       | 作用      | 权限 |
|----------|---------|----|
| #广播通知    | 群发所有群聊  | 主人 |
| #白名单广播通知 | 群发白名单群聊 | 主人 |
| #黑名单广播通知 | 群发黑名单群聊 | 主人 |
##### 设置项
| 设置项                     | 作用                                | 值               |
|-------------------------|-----------------------------------|-----------------|
| 发送消息延迟(delays)          | 群发消息时每个群聊的发送时间间隔                  | true\false（是或否） |
| 发送消息随机延迟(random_delays) | 需开启“发送消息延迟”，开启后随机发送消息间隔时间，减小被检测概率 | true\false（是或否） |
| 发送消息延迟(Nnumber)         | 需开启“发送消息延迟”，发送消息时间间隔，单位为毫秒        | 数字              |
| cfg.isMakeMsgList | 广播完成后将所有结果合并转发而不是实时返回 | true\false（是或否） |
| cfg.isOnlyResult | 只发送简易广播结果 例如【成功：7 失败：3】 | true\false（是或否） |




---
## 违规图片管制.js
##### 安装
```
curl -o "./plugins/example/违规图片管制.js" "https://gitee.com/qiannqq/yunzai-plugin-JS/raw/master/JS/wgtpgz.js"
```
##### 简介
本插件对接的是百度云内容审核，包括鉴权方法以及接口调用。<br>
本插件适合对群管理要求严格的群使用<br>
 **主要用于对群内群友发送的图片进行人工智能审核，不合规的图片会被撤回并警告** <br>
本插件会对图片的MD5值进行对比，相同图片MD5会读取已存储的审核结果，避免额外的资源浪费
##### 百度云内容审核 APIkey 和 Secreykey 获取和配置方法
借用ap插件的文档链接，配置方法在这→[https://ap-plugin.com/config/docs12](https://ap-plugin.com/config/docs12)
##### 违规图片的配置
违规图片的范畴需自行在百度云中配置，本插件仅处理百度云返回的结果是否为 **不合规** 
##### 指令以及其作用
(PS:列表中的所有指令，若未特别备注，均为主人权限可使用)
| 指令    | 作用                                   | 发布版本 |
|-------|--------------------------------------|------|
| #图片加白 | 适用于误审误判的图片，将其列为“合规”图片(使用本指令需一并携带图片)  | v1.1.0 |
| #图片拉黑 | 适用于误审误判的图片，将其列为“不合规”图片(使用本指令需一并携带图片) | v1.1.0 |

##### 设置项
| 设置项              | 作用                       | 值    |
|------------------|--------------------------|------|
| 白名单(whitelist)   | 以防止滋生额外费用，仅配置的群聊才进行图片管控  | 数字   |
| Api_Key          | 百度云应用的API Key            | 英 数字 |
| Secret_Key       | 百度云应用的Secret Key         | 英 数字 |
| 警告次数(warn_not)   | 当用户触发警告次数的上限时将对其进行禁言处理   | 数字   |
| 禁言时长(muted_time) | 当用户触发警告次数上限时对其禁言的时间，单位为秒 | 数字   |

<details><summary>更新日志</summary>

- [`点击查看`初代版本发布](https://gitee.com/qiannqq/yunzai-plugin-JS/commit/02d64a04db51f96f82bbd747734734e7be97b714)
- [`点击查看`v1.1.0更新日志](https://gitee.com/qiannqq/yunzai-plugin-JS/commit/117e7b664a70a9d4cb4f5caf16b849c8cd593d51)

</details>


---
## 加群申请自动处理.js
##### 安装
```
curl -o "./plugins/example/加群申请自动处理.js" "https://gitee.com/qiannqq/yunzai-plugin-JS/raw/master/JS/jqsqzdcl.js"
```
#### 简介
QQ自带的答案判断无法判断多个答案，且可能正确的答案无法被判断，还有判断错误的可能。<br>
可以使用该插件判断多个答案，可能正确的答案也不会直接拒绝
##### 设置项
| 设置项              | 作用                 | 值     |
|------------------|--------------------|--------|
| 问题(wenti)        | 填写群问题              | 中 英 字符 |
| 答案(daan)         | 填写需要判断的答案          | 中 英 字符 |
| 开启判断的群号(groupid) | 仅开启在该名单中的群自动处理加群事件 | 数字     |


---
## 次权限管理.js
##### 简介
**次权限**类似于**2级主人**，**2级主人**的所有权限仅在配置的群聊生效。**2级主人**可在配置的群拉黑QQ，解除被拉黑的QQ，禁用指定指令，解除被禁用的指令等。(仅在配置的群聊中生效)<br>
什么？看不懂？打个比方，**假如主人是“频道主”**，那么**2级主人就是“子频道管理员”**。<br>
配置二级主人可在resources/userblack/userconfig.txt中按照下面的格式配置，也可以在需要配置的群聊发送“#新增权限用户+QQ”<br>
该JS的屏蔽逻辑是通过超高的优先级抢掉指令，所以对“戳一戳”等特殊方法的功能无效。
##### 安装
```
curl -o "./plugins/example/次权限管理.js" "https://gitee.com/qiannqq/yunzai-plugin-JS/raw/master/JS/cqxgl.js"
```
在resources下创建文件夹userblack文件夹，并在userblack文件夹内创建三个txt文件<br>
文件名分别为：userblack.txt、userconfig.txt、userbanGN.txt
##### 权限配置方式
按照 [👉此格式👈](https://gitee.com/qiannqq/yunzai-plugin-JS/blob/master/userblack/userconfig.txt) 配置次权限(ban权限)<br>
其中，123456789代表权限生效的qq号，789456123代表权限生效的群聊，按照此格式配置即可<br>
 **注意：需要在最后一行权限配置加上一个回车，保证txt空出来一行。不进行此操作可能会导致权限无法生效** 
<br>主人并不具有次权限，需要手动添加
##### 指令以及其作用
（PS：指令中的“QQ号”需要替换为实际用户QQ号，不能加空格）
| 指令          | 作用            |
|-------------|---------------|
| #云崽banQQ号   | 该用户在该群无法使用机器人 |
| #云崽unbanQQ号 | 解除对该用户的限制    |
| #云崽禁用关键词 | 包含关键词的指令在该群不生效 |
| #云崽解禁关键词 | 包含关键词的指令在该群解禁 |
| #移除权限用户QQ号 | 移除该QQ号用户当前群聊的次权限(该指令仅主人、在群聊时可用) |
| #新增权限用户QQ号 | 赋予该QQ号用户当前群聊的次权限(该指令仅主人、在群聊时可用) |


---
## 关于.js
##### 安装
```
curl -o "./plugins/example/关于.js" "https://gitee.com/qiannqq/yunzai-plugin-JS/raw/master/JS/gy.js"
```

<details><summary>缺少依赖axios</summary>

执行`pnpm i axios -w`后重启喵崽即可<br>
`ERR_PNPM_INCLUDED_DEPS_CONFLICT`报错解决方法

```
pnpm cache clear #清理缓存
pnpm install -g pnpm #更新pnpm
pnpm update #更新依赖项
```

如果都不行的话建议把node_modules文件夹删了，重新安装所有依赖

</details>

##### 简介
关于云崽 显示你的bot详情信息，包括连续运行时间、累计运行时间、框架信息、平台、插件数、JS数、主人qq等。

<details><summary>功能示例</summary>

![关于](demonstrate/关于.png)

</details>

##### 食用方法
| 指令    | 作用                     |
|-------|------------------------|
| #关于(云崽) | 显示bot详情信息 |

##### 设置项
| 设置项                | 作用                          | 值                |
|--------------------|-----------------------------|------------------|
| 机器人名字(botname) | #关于(机器人名字)触发词，关于详情图片顶部名字 | 中 英 字符 等 |
| 机器人创建时间(targetDate) | 用于显示机器人累计运行天数，格式为20xx-xx-xx | 数字 |
| files(不建议自定义设置) | 插件包路径 | 英 字符 |
| folderPath1(不建议自定义设置) | JS路径 | 英 字符 |

---
## 云崽上下班.js
##### 安装
```
curl -o "./plugins/example/云崽上下班.js" "https://gitee.com/qiannqq/yunzai-plugin-JS/raw/master/JS/yzsxb.js"
```
##### 简介
云崽上下班，方便调试的时候屏蔽所有指令(想不到其它用途了<br>
 **注意：第一次安装插件必须要发送一次#云崽上班！！！** 
##### 食用方法
| 指令    | 作用                     |
|-------|------------------------|
| #云崽上班 | 接受所有指令 |
| #云崽下班 | 屏蔽所有指令 |
##### 设置项
| 设置项       | 作用                     | 值  |
|-----------|------------------------|----|
| 测试群(test) | 下班后测试群可以正常使用机器人，方便测试插件 | 数字 |


---
## 关键词提醒.js
##### 安装
```
curl -o "./plugins/example/关键词提醒.js" "https://gitee.com/qiannqq/yunzai-plugin-JS/raw/master/JS/gjctx.js"
```
##### 简介
在Bot所在的群聊内，如果有人提到了你设置的关键词，比如：屑千奈太屑了，即包含“千奈”关键词，Bot会提示消息至第一个主人（私聊）<br>
<details><summary>功能示例</summary>

![关键词提醒](demonstrate/gjctx.png)

</details>

##### 食用方法
修改或新增第23行''内要提示的关键词即可
##### 黑名单屏蔽设置方法(在黑名单中的qq号不触发关键词提醒)
修改或新增第6行''内的qq号即可，用半角逗号隔开
| 设置项                | 作用                          | 值                |
|--------------------|-----------------------------|------------------|
| 黑名单用户(banid)         | 在黑名单的qq号，其触发关键词也不会通知给主人     | 数字         |
| 卡片消息通知(isKapian)   | 是否通知包含关键词的卡片消息（包括转发消息、b站分享等） | true\false （是或否） |
| Q群管家通知（isQguanjia） | 是否通知Q群管家发送的包含关键词的消息         | true\false （是或否） |



---
## 今日老婆.js
##### 安装
```
curl -o "./plugins/example/今日老婆.js" "https://gitee.com/qiannqq/yunzai-plugin-JS/raw/master/JS/jrlp.js"
```
 **指令以及其作用：** 
| 指令      | 作用                     |
|---------|------------------------|
| (#)今日老婆 | 在群中随机抽取一位群友当老婆，每天只能抽一次 |

<details><summary>功能示例</summary>

![今日老婆](demonstrate/jrlp.png)

</details>


---
## 模拟高考查分.js
##### 安装
```
curl -o "./plugins/example/模拟高考查分.js" "https://gitee.com/qiannqq/yunzai-plugin-JS/raw/master/JS/mngkcf.js"
```

<details><summary>缺少依赖axios</summary>

执行`pnpm i axios -w`后重启喵崽即可<br>
`ERR_PNPM_INCLUDED_DEPS_CONFLICT`报错解决方法

```
pnpm cache clear #清理缓存
pnpm install -g pnpm #更新pnpm
pnpm update #更新依赖项
```

如果都不行的话建议把node_modules文件夹删了，重新安装所有依赖

</details>

 **指令以及其作用：** 
<br>
| 指令    | 作用                                     |
|-------|----------------------------------------|
| (#)查分 | 语数英物化政六科分数随机抽取，每个人只能抽一次，重复使用可查看已抽取到的分数 |

<details><summary>功能示例</summary>

![查分](demonstrate/%E6%9F%A5%E5%88%86.png)

</details>


##### 其他
语数英保底分数40，物化政保底分数30。（语数英满分150，物化政满分100）

---
## 屏蔽指定指令，只有主人才可执行.js
##### 安装
下载 屏蔽指定指令，只有主人才可执行.js，将js放入Yunzai-Bot/plugins/example文件夹里。
##### 编辑你要屏蔽的指令和回复内容（回复内容可以不改的）
用记事本或者其他编辑工具打开js，在第18行里修改你要屏蔽的指令正则，修改完ctrl+s保存即可。
<br>
因为要做示例用，所以默认屏蔽了 #椰奶状态(pro) 和 #运行状态，可以照着这两个写。
<br>
第26行可以修改屏蔽所回复的消息，也可以直接注释(删掉)那一行不回复。
##### 修改判断权限（这玩意也没必要改）
第25行可以修改判断的权限，怎么改就看你了。（已知权限有：主人、群主、群管）

---

## 每日打卡.js
```
curl -o "./plugins/example/每日打卡.js" "https://gitee.com/qiannqq/yunzai-plugin-JS/raw/master/JS/mrdk.js"
```
 **指令以及其作用:** 
<br>
| 指令 | 作用 |
|----|----|
| 每日打卡 | 随机抽取0-100之间的数字作为幸运值，每天只能抽取一次（重复使用可查看当天抽到的幸运值） |
| 今日欧皇 | 查看今天首个幸运值为100的欧皇 |
| 让我看看你的卡@群友 | 看看群友的幸运值 |

<details><summary>功能示例</summary>

每日打卡<br>
![每日打卡](demonstrate/%E6%89%93%E5%8D%A1.png)
<br>今日欧皇<br>
![今日欧皇](demonstrate/%E4%BB%8A%E6%97%A5%E6%AC%A7%E7%9A%87.png)
<br>让我看看你的卡<br>
![让我看看你的卡](demonstrate/%E8%AE%A9%E6%88%91%E7%9C%8B%E7%9C%8B%E4%BD%A0%E7%9A%84%E5%8D%A1.png)

</details>

---

## 抽签.js
```
curl -o "./plugins/example/抽签.js" "https://gitee.com/qiannqq/yunzai-plugin-JS/raw/master/JS/cq.js"
```
指令以及其作用:
<br>
| 指令 | 作用 |
|----|----|
| (#)抽签、(#)求签、今日运势 | 随机抽取签文、宜和忌、小故事。每天只能抽一次，重复使用可查看当天抽到的签。 |
<details><summary>功能示例</summary>

![抽签](demonstrate/%E6%8A%BD%E7%AD%BE.png)

</details>

---
## 表情合成.js

```
curl -o "./plugins/example/BQHC.js" "https://gitee.com/qiannqq/yunzai-plugin-JS/raw/master/BQHC.js"
```
**表情合成已停止维护，欢迎大佬PR**

#### 使用说明

无需重启             
表情合成.js 发送 表情合成帮助 获取指令详情（请勿加#字符）

---
## 其它
 - 严禁用于任何商业用途(盈利等)和非法行为
 -  **感谢 [@酸菜闲鱼](https://gitee.com/suancaixianyu) 提供指导** 
 - 图片托管：https://moeyy.cn/img/
 - 抽签API：http://shanhe.kim/api/za/chouq.php
 - 表情合成API：http://free.wqwlkj.cn
 - [我的个人博客，欢迎光临](http://blog.moqy.top)
 - 点个star或者[爱发电](https://afdian.net/a/QianNQQ)，是你支持本项目继续维护的动力~
 - 如需搬运或二改我库中插件，请标明原作者并向我留言，不需要得到我的回复哦(电子邮件或平台内都行)

## 感谢赞助
| Nickname |
|---|
| 皮梦 |
| 爱发电用户_pRBn |
| 休比 |
| 枭 |
| 来来 |
| 慕晚. |


