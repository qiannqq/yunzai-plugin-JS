#!/bin/bash

# 设置新的daemon.json文件内容
new_content='{
  "dns": ["223.5.5.5","8.8.8.8", "8.8.4.4"]
}'

# 检查是否存在/etc/docker目录，如果不存在则创建
if [ ! -d "/etc/docker" ]; then
    sudo mkdir -p /etc/docker
fi

# 将新内容写入到daemon.json文件
echo "$new_content" | sudo tee /etc/docker/daemon.json > /dev/null

# 重启Docker服务使更改生效
sudo systemctl restart docker

echo "Docker服务已重启并应用新的配置。"